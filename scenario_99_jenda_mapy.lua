-- Name: Mapy.cz - first attempt
-- Description: Prvni pokus v EE pro Mapy.cz
--- Nic
-- Type: Mission


-- Init is run when the scenario is started. Create your initial world 
function init()
	-- EARTH N20 - 15*20000, 8*20000
	earth = Planet():setPosition(random(15*20000,16*20000),random(8*20000,9*20000))
	earth:setPlanetSurfaceTexture("planets/planet-1.png")
	earth:setPlanetAtmosphereTexture("planets/atmosphere.png")
	earth:setFaction("Human Navy")
	earth:setPlanetRadius(3000)
	earth:setCallSign("Earth")
	
	-- EARTH SPACE DOCK - NEW YORK SPACE DOCK M20 - 15*20000, 7*20000
	earth_sd_x = random(15*20000,16*20000)
	earth_sd_y = random(7*20000,8*20000)
	earth_sd = SpaceStation():setTemplate("Huge Station")
	earth_sd:setPosition(earth_sd_x,earth_sd_y)
	earth_sd:setFaction("Human Navy")
	earth_sd:setCallSign("New York Spacedock")


	-- Players ships
	-- Main battle ship - M20
	main_ship = PlayerSpaceship():setFaction("Human Navy")
	main_ship:setTemplate("Atlantis")
	main_ship:setCallSign("USS Constitution")
	main_ship:setRepairDocked(true)
	main_ship:setPosition(earth_sd_x - 2000, earth_sd_y)
	main_ship:commandDock(earth_sd)
	
	fighter_ship = PlayerSpaceship():setFaction("Human Navy")
	fighter_ship:setTemplate("Phobos M3P")
	fighter_ship:setCallSign("USS Akira")
	fighter_ship:setRepairDocked(true)
	fighter_ship:setPosition(earth_sd_x, earth_sd_y - 2000)
	fighter_ship:commandDock(earth_sd)

	-- Science ship
	
	-- Asteroids field

	-- Nebula

	-- Some enemy around science ship



	-- Diplomatic ship

	-- Escort ship for diplomatic

	-- Some enemy for diplomatic



	-- Battle ship for defend boundary of federation

	-- Atacking enemy



	-- GM functions
	addGMFunction("Win", function()
		victory("Human Navy");
	end)


	-- Some others station



	-- POZN
	-- Research station
	--research_station = SpaceStation():setTemplate("Medium Station"):setFaction("Human Navy"):setPosition(start_x + 1000, start_y - 1000):setCallSign("Regula I")

	-- Council station
	--federation_council_station = SpaceStation():setTemplate("Huge Station"):setFaction("Human Navy"):setPosition(fin_x, fin_y):setCallSign("Yorktown")

	-- Create research transport ship
	--transport_ship = CpuShip():setTemplate("Flavia"):setFaction("Human Navy"):setPosition(start_x + 700, start_y - 700)

	-- Independent station for support
	--SpaceStation():setTemplate("Large Station"):setFaction("Independent"):setPosition(len_x*0.4+random(-3000,3000), len_y*0.4+random(-3000,3000)):setCallSign("Babylon 5")


	--Grab a reference to Nerva Station station.
	--enemy_station = SpaceStation():setTemplate("Large Station"):setFaction("Exuari")
	--enemy_station_x = len_x*0.7+random(-2000,2000)
	--enemy_station_y = len_y*0.7+random(-2000,2000)
	--enemy_station:setPosition(enemy_station_x, enemy_station_y):setCallSign("Nerva Station")

	--Nebulae that hide the enemy station.
	--for i=1,6 do
	--	Nebula():setPosition(enemy_station_x + random(-5000, 5000), enemy_station_y + random(-5000, 5000))
	--end

	--Random nebulae in the system
	--for i=1,80 do
	--	Nebula():setPosition(random(-150000, 150000), random(-150000, 150000))
	--end


	-- Defender of enemy base
	--for i=1,10 do
	----	CpuShip():setTemplate("MT52 Hornet"):setFaction("Exuari"):setPosition(enemy_station_x+random(-1000,1000), enemy_station_y+random(-1000,1000)):orderDefendTarget(enemy_station)
	--end

	--Create 100 asteroids
	--for asteroid_counter=1,300 do
	--	Asteroid():setPosition(len_x*0.2+random(-15000, 15000), len_y*0.2+random(-15000, 15000))
	----	VisualAsteroid():setPosition(len_x*0.2+random(-15000, 15000), len_y*0.2+random(-15000, 15000))
	--end


	-- Create some attack ship
	--CpuShip():setTemplate("Flavia"):setFaction("Exuari"):setPosition(start_x+random(4000,6000), enemy_station_y+random(-6000,-4000)):orderRoaming()
	--CpuShip():setTemplate("MT52 Hornet"):setFaction("Exuari"):setPosition(start_x+random(4000,6000), enemy_station_y+random(-6000,-4000)):orderRoaming()


end

function update(delta)
	-- Mission progress


	--When the player ship is destroyed, call it a victory for the Exuari.
	--if not player:isValid() then
	--	victory("Exuari")
	--end
	
	--if distance(transport_ship, federation_council_station) < 500 then
	--	victory("Human Navy")
	--end

	--When Omega station is destroyed, call it a victory for the Human Navy.
	--if not enemy_station:isValid() then
	--	victory("Human Navy")
	--end
end

function distance(obj1, obj2)
	local x1, y1 = obj1:getPosition()
	local x2, y2 = obj2:getPosition()
	local xd, yd = (x1 - x2), (y1 - y2)
	return math.sqrt(xd * xd + yd * yd)
end
